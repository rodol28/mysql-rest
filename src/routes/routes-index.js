const router = require('express').Router();
const User = require('../models/model-users');

router.get('/users', (req, res) => {
    User.getUser((err, data) => {
        res.status(200).json(data);
    });
});

router.post('/users', (req, res) => {
    
    const userData = {
        id: null,
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
    };

    User.insertUser(userData, (err, data) => {
        if(data && data.insertId) {
            res.json({
                success: true,
                msg: 'User added successfully',
                data: data
            });
        } else {
            res.status(500).json({
                success: false,
                msg: 'Failed to insert user'
            });
        }
    });

});


router.put('/users/:id', (req, res) => {
    const userData = {
        id: req.params.id,
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
        updated_at: 'CURRENT_TIMESTAMP'
    };

    User.updateUser(userData, (err, data) => {
        if(data && data.msg) {
            res.json(data);
        } else {
            res.json({
                success: false,
                msg: 'Failed to update user'
            });
        }
    });
});

router.delete('/users/:id', (req, res) => {
    const id_user = req.params.id;

    User.deleteUser(id_user, (err, data) => {
        if(data && data.msg == 'deleted successfully') {
            res.json(data);
        } else {
            res.json({
                success: false,
                msg: 'Failed to delete user'
            });
        }
    });
});

module.exports = router;