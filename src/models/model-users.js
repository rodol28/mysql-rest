const mysql = require('mysql');

connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'testapimysql'
});


let userModel = {};

userModel.getUser = (callback) => {
    if(connection) {
        connection.query('SELECT * FROM `users` ORDER BY id_user', (err, rows) => {
            if(err) {
                throw err;
            } else { 
                callback(null, rows)
            }
        });
    }
};


userModel.insertUser = (userData, callback) => {
    if(connection) {
        let sql = `INSERT INTO users (username, password, email) VALUES ('${userData.username}', '${userData.password}', '${userData.email}');`;

        connection.query(sql, (err, result) => {
            if(err) {
                throw err;
            } else { 
                callback(null, {
                    'insertId': result.insertId
                })
            }
        });
    }
};

userModel.updateUser = (userData, callback) => {
    if(connection) {
        let sql = `UPDATE users SET username='${userData.username}', password='${userData.password}', email='${userData.email}', updated_at=${userData.updated_at} WHERE id_user=${userData.id}`;
        
        connection.query(sql, (err, result) => {
            if(err) {
                throw err;
            } else{
                callback(null, {
                    msg: 'updated successfully'
                })
            }
        });
    }
};

userModel.deleteUser = (id_user, callback) => {
    if(connection) {
        const sql = `DELETE FROM users WHERE id_user=${id_user}`;

        connection.query(sql, (err, result) => {
            if(err) {
                throw err;
            } else {
                callback(null, {
                    msg: 'deleted successfully'
                });
            }
        });
    }
};


module.exports = userModel;

