const express = require('express');
const router = require('./routes/routes-index.js');
const morgan = require('morgan');
const bodyParser = require('body-parser');

const app = express();

//settings 
app.set('port', process.env.PORT || 3000);

//middleware
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(router);



app.listen(app.get('port'), () => {
    console.log(`Server listening on port ${app.get('port')}`);
});